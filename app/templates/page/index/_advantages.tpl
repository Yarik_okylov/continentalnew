<div class="advantages" style="background-image: url(/static/images/advantages-back.png)">
    <div class="row">
        <div class="column">
            <h2 class="advantages__title">
                Преимущества
            </h2>
            <ul class="advantages__list">
                <li class="advantages__item">
                    {icon: raw "size-icon"}
                    <div class="advantages__content">
                        <span class="advantages__text">
                            Возможность заказать
                            <br>любой размер
                        </span>
                        <a href="" class="advantages__link">
                            Подобрать шины
                            <span class="advantages__hand-right">{icon: raw "hand-right"}</span>
                        </a>
                    </div>
                </li>
                <li class="advantages__item">
                    {icon: raw "price-icon"}
                    <div class="advantages__content">
                        <span class="advantages__text">
                            Низкая цена
                        </span>
                        <a href="" class="advantages__link">
                            Подобрать шины
                            <span class="advantages__hand-right">{icon: raw "hand-right"}</span>
                        </a>
                    </div>
                </li>
                <li class="advantages__item">
                    <img class="advantages__image" src="/static/images/guarantee-icon.png" alt="">
                    <div class="advantages__content">
                        <span class="advantages__text">
                            Пожизненная гарантия
                        </span>
                        <a href="" class="advantages__link">
                            Подробнее о гарантии
                            <span class="advantages__hand-right">{icon: raw "hand-right"}</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>