<div class="index-catalog">
    <div class="row">
        <div class="column">
            <ul class="index-catalog__switch smart-tabs">
                <li class="index-catalog__tab active">
                    <a href="#tab-1" class="index-catalog__tab-container">
                        <span class="index-catalog__tab-icon">{icon: raw "summer"}</span>
                        <span class="index-catalog__tab-text">Летние шины</span>
                    </a>
                </li>
                <li class="index-catalog__tab">
                    <a href="#tab-2" class="index-catalog__tab-container">
                        <span class="index-catalog__tab-icon">{icon: raw "winter"}</span>
                        <span class="index-catalog__tab-text">Зимние шины</span>
                    </a>
                </li>
            </ul>
            <div class="index-catalog__wrapper smart-contents">
                <div class="index-catalog__content smart-content active">
                    <ul class="product-list">
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li><li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li><li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="product-list__item">
                            <div class="product-card">
                                <a class="product-card__main-link" href="">
                                    <span class="product-card__image-wrapper">
                                        <img src="/static/images/test-tyre-image.png" alt="" class="product-card__image">
                                    </span>
                                    <span class="product-card__name text-center">PremiumContact™ 6</span>
                                    <span class="product-card__description text-center">Суперспортивная шина  с превосходной управляемостью  для максимального удовольствия  от вождения</span>
                                </a>
                                <div class="product-card__bottom">
                                    <div class="product-card__size-container">
                                        <div class="product-card__size-title">
                                            <span class="product-card__icon">{icon: raw "product-size-icon"}</span>
                                            <span class="product-card__size-text">Размеры:</span>
                                        </div>
                                        <ul class="product-card__line-size">
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">15"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">16"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">17"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">18"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">19"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">20"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">21"</a>
                                            </li>
                                            <li class="product-card__size-item">
                                                <a href="" class="product-card__size-link">22"</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="" class="product-card__button button">
                                        <span class="product-card__button-text">Узнать больше</span>
                                        <span class="product-card__button-icon">{icon: raw "hand-right-black"}</span>
                                    </a>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>