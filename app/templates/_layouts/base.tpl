<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    {* Title, description, keywords *}
    {block 'seo'}
        {render_meta:raw}
    {/block}

    <link rel="stylesheet" href="{$.assets_public_path('main.css', 'frontend')}">

    {* Another head information *}
    {block 'head'}{/block}
</head>
<body>
<div class="layout">
    <div class="layout__main">
        <header class="header layout__header">
            <div class="row">
                <div class="column large-12">
                    <div class="header__line">
                        <div class="header__logo">
                            <img src="/static/images/header_logo.png" alt="" class="header__logo-img">
                        </div>
                        <div class="header__data">
                            <div class="header__main">
                                <div class="header__feedback">
                                    <a href="tel:_7..." class="header__phone header__phone_red">
                                        {icon:raw "phone"}
                                        <span class="header__number">+7 (8332) 66-10-10</span>
                                    </a>
                                    <a class="header__button button button_transparent" href="#">
                                        Заказать звонок
                                    </a>
                                </div>
                                <div class="header__cart">
                                    {icon:raw "cart"}
                                    <span class="header__quantity">123 шт./1 200 000 Р</span>
                                </div>
                            </div>
                            <div class="header__menu menu">
                                <ul class="menu__list">
                                    {set $menu = [
                                    'Летние шины' => '/',
                                    'Зимние шины' => '/',
                                    'О дистрибьюторе' => '/',
                                    'Гарантии' => '/',
                                    'Инновации' => '/',
                                    'Вопрос/Ответ' => '/',
                                    'Контакты' => '/',
                                    ]}
                                    {foreach $menu as $name => $url}
                                    <li class="menu__item">
                                        <a href="{$url}" class="menu__link">
                                            <span class="menu__link-text">{$name}</span>
                                        </a>
                                    </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="layout__content">
            <img src="/static/images/background.png" alt="">
            {block 'content'}

            {/block}
        </div>
    </div>
    <footer class="footer layout__footer">
        <div class="row">
            <div class="column large-12">
                <div class="footer__line">
                    <div class="footer__main">
                        <div class="footer__content">
                            <div class="footer__title">Шины</div>
                            <ul class="footer__menu">
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Летние шины
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Зимние шины
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Гарантии
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Инновации
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__content">
                            <div class="footer__title">Сервис</div>
                            <ul class="footer__menu">
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        О дистрибьютере
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Новости
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Оплата и доставка
                                    </a>
                                </li>
                                <li class="footer__menu-item">
                                    <a class="footer__menu-link" href="#">
                                        Вопрос/ответ
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer__content">
                            <div class="footer__title">Контакты</div>

                            <div class="footer__feedback">
                                <a class="footer__phone" href="tel:">
                                    <span class="footer__icon">{icon:raw "phone-footer"}</span>
                                    <span>+7 (8332) 66-10-10</span>
                                </a>
                                <a class="footer__mail" href="mailto:">
                                    <span class="footer__icon">{icon:raw "mail-footer"}</span>
                                    <span>mail@continental.store</span>
                                </a>
                                <a class="footer__button button button_transparent">
                                    Заказать звонок
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="footer__logo">
                        <img src="/static/images/footer_logo.png" alt="" class="footer__logo-img">
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

    {block 'core_js'}
        <script src="{$.assets_public_path('vendors.js', 'frontend')}"></script>
        <script src="{$.assets_public_path('main.js', 'frontend')}"></script>
    {/block}
    {render_dependencies_js:raw}
    {render_inline_js:raw}
    {render_dependencies_css:raw}
    {render_inline_css:raw}
    {block 'js'}

    {/block}
</body>
</html>