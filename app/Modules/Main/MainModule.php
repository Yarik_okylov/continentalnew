<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 04.07.18
 * Time: 11:56
 */
namespace Modules\Main;

use Phact\Module\Module;
use Modules\Admin\Traits\AdminTrait;

class MainModule extends Module
{
    use AdminTrait;

    public static function getSettingsModel()
    {
        return new MainSettings();
    }
}
