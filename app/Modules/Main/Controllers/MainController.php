<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 04.07.18
 * Time: 11:21
 */
namespace Modules\Main\Controllers;

use Phact\Controller\Controller;

class MainController extends Controller
{
    public function index()
    {
        echo $this ->render('page/index/index.tpl');
    }
}