<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 04.07.18
 * Time: 11:59
 */
namespace Modules\Main\Models;

use Phact\Orm\Model;

class MainSettings extends Model
{
    public static function getFields() 
    {
        return [
        
        ];
    }
    
    public function __toString() 
    {
        return (string)$this->name;
    }
} 